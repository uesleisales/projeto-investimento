<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateUsersTable.
 */
class CreateUsersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
			$table->increments('id');

			//dados da pessoa
			$table->char('cpf' , 20)->unique()->nullable(); //nullalbe porque pode ser nulo
			$table->char('name', 50);
			$table->char('phone', 20);
			$table->date('birth')->nullable();
			$table->char('gender' , 1)->nullable();
			$table->text('noutes')->nullable();
			

			//dados de autenticação
			$table->string('email',80)->unique();
			$table->string('password',254)->nullable(); //Nullable porque va iser possivel logar com rede social
			
			//Permissão
			$table->string('status')->default('active'); //Deixa por padrão ativo
			$table->string('permission')->default('app.user'); //Deixa por padrão ativo
			
			$table->rememberToken();
			$table->timestamps(); //Ele vai criar os campos created_at e update_at
			$table->softDeletes(); //vai criar o campo deleted_at (valor continua no banco mais o laravel vai entender como apagado)
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{   
		Schema::table('users', function(Blueprint $table) {
			
		});
		Schema::drop('users');
	}
}
