<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSocialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_socials', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsined(); //Chave estrangeira para user  - unsined é pra ficar do mesmo tipo que o campo id (ele fica nesse tipo quando da auto_increment)
            $table->string('social_network'); //Vai ter varias redes sociais
            $table->string('social_id') ;     //Id único da rede social correspondente 
            $table->string('social_email');
            $table->string('social_avatar');

            $table->timestamps();      
            
            //$table->foreign('user_id')->references('id')->on('users'); //campo user_id faz referência ao campo id da tabela users
            //$table->foreign('social_email')->references('email')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   /*
        Schema::table('user_socials', function (Blueprint $table){
            $table->dropForeign('user_socials_user_id_foreign');   //Necessário remover os relacionamentos antes de remover a tabela
            $table->dropForeign('user_socials_social_email_foreign');
            //Nome do relacionamento :nomedatabela_nomedocampo_foreign
        });

        */
        //Schema::dropIfExists('user_socials');

        Schema::table('user_socials', function(Blueprint $table) {
			
		});
        Schema::drop('users_socials');
        
    }
}
