<?php

use Illuminate\Database\Seeder;
use App\Entities\User; //Adicionado

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
           'cpf'       =>'85920220538', 
           'name'      =>'Admin',                                          
           'phone'     =>'988314261',        
           'birth'     =>'1996-02-03',       
           'gender'    =>'M',
           'noutes'    =>'Usuario master',        
           'email'     =>'wesleijt@hotmail.com',       
           'password'  => env('PASSWORD_HASH') ? bcrypt('magalhaes10') : 'magalhaes10',
          ]);
    }
}
