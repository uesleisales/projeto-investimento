<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    use Notifiable;
    use SoftDeletes;

    public $timestamp = true; 
    protected $table     = 'user_groups';
    protected $fillable = ['user_id', 'group_id' ,  'permission']; 
   
    protected $hidden = []; 

    
}
