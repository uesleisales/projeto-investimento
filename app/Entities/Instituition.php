<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Instituition.
 *
 * @package namespace App\Entities;
 */
class Instituition extends Model implements Transformable
{
    use TransformableTrait;
    protected $fillable = ['name'];
    protected $timestamp= true;


    public function groups(){
        //Uma instituição pode ter varios grupos
        return $this->hasMany(Group::class);
    }

    public function products(){
        return $this->hasMany(Product::class);
    }

}
