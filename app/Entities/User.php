<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;  //Adicionada
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    public $timestamp = true; //Como ta true created_at , deleted_at e update_at irão funcionar
    protected $table     = 'users'; //Nome da tabela
    protected $fillable = ['id' , 'cpf', 'name' , 'phone' , 'birth' , 'gender' ,'noutes' , 'email' , 'status' ,'permission']; 
    //$fillable , explicação: 
    //Os parãmetros irão assim $usuario = new User(['ueslei' ,'wesleijt@hotmail.com' ,'magalhaes10']);
    //Ao invés de :
    //$usuario = new User();
    //$usuario->name ="Ueslei";
    //$usuario->password = "magalhaes10";
    //OBS: só não precisa passar os campos sensiveis como id , created_at , token .... 

    protected $hidden = ['password', 'remember_token']; //pra não ser um atributo visivel

    public function groups()
    {  
        return $this->belongsToMany(Group::class , 'user_groups'); //Relacionamento N PARA N , o segundo parametro a a tabela de apoio ou intemediaria entre Usuarios e Grupos
    }

    

    public function moviments()
    {
        return $this->hasMany(Moviment::class);
    }


    public function setPasswordAttribute($value){

        $this->attributes['password'] = env('PASSWORD_HASH') ? bcrypt($value) : $value;
    }

    

    //$user->cpf            -- na view vai trazer apenas números
    //$user->Formatted_cpf   -- na view vai trazer o campo formatado

    public function getFormattedCpfAttribute(){ //Colocando o Formated o campo não é subscrito com a mascara no banco

        $cpf = $this->attributes['cpf'];

        return substr($cpf , 0 , 3) . '.' .substr($cpf , 3 , 3) . '.' .substr($cpf , 7 , 3) . '-' .substr($cpf , - 2) ;
    }

    public function getFormattedPhoneAttribute(){

        $phone = $this->attributes['phone'];

        return "(" . substr($phone , 0 , 2) . ")" .substr($phone , 2 , 4) .  "-" . substr($phone , 0 , - 4 );
    }


    public function getFormattedBirthAttribute(){
        
        $date = date("d/m/Y" ,strtotime($this->attributes['birth']));
        return $date;
    }

    public function getFormattedStatusAttribute(){
        
        $array = array(
            'active' => 'Ativo'
        );
        
        return $array[$this->attributes['status']];
    }


    public function getFormattedPermissionAttribute(){
        
        $array = array(
            'app.user' => 'Usuário'
        );

        return $array[$this->attributes['permission']];
    }


}

