<?php 

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;  //Adicionada


class UserSocial extends Model{
    use Notifiable;
    use SoftDeletes;

    public $timestamp = true; //Como ta true created_at , deleted_at e update_at irão funcionar
    protected $table     = 'users'; //Nome da tabela
    protected $fillable = ['user_id' , 'social_network' , 'social_id' ,'social_email' , 'social_avatar']; 
    protected $hidden = []; //Nenhum campo precisa ser escondido nenhum dado
}