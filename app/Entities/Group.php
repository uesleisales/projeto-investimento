<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/*
*================================================================================
*Um grupo vai pertecer a uma instituição e terá uma pessoa responsável pelo mesmo
*================================================================================
*/
 
class Group extends Model implements Transformable
{
    use TransformableTrait;
    

    protected $fillable = ['name' , 'user_id' , 'instituition_id'];

    public function getTotalValueAttribute()
    {   
        
        $entrada =  $this->moviments()->applications()->sum('value');
        $saida   =  $this->moviments()->outFlows()->sum('value');

        return $entrada - $saida;
    }

    public function user(){
        return $this->belongsTo(User::Class); //Um grupo pertence a um usuário
       //return $this->belongsTo(User::Class , 'user_id'); //Se o nome do método não for o mesmo da tabela os parametros precisa ser passados
    }

    
    public function users(){ //Diferente do de cima
        return $this->belongsToMany(User::class , 'user_groups'); //Relacionamento N PARA N , o segundo parametro a a tabela de apoio ou intemediaria entre Usuarios e Grupos
    }

    public function instituition(){
        return $this->belongsTo(Instituition::Class); //Pertence a uma instituição também
    }

    public function moviments(){
        return $this->hasMany(Moviment::class);
    }
}
