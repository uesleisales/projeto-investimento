<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;


class Product extends Model implements Transformable
{
    
    protected $fillable = ['instituition_id' , 'name' , 'description' , 'index' , 'interest_rate'];


    public function instituition()
    {
        return $this->belongsTo(Instituition::class);
    }

    public function valueFromUser(User $user)
    {       
             //================ applications() e product() são scopeApplications()  e scopeProduct() definidos no Entitie Moviments
             //usando scope evita de fazer varios wheres aqui

             $inflows = $this->moviments()->product($this)->applications()->sum('value'); //sum é soma 
             $outflows= $this->moviments()->product($this)->outFlows()->sum('value'); //sum é soma 
             
            return $inflows - $outflows;
    }

    public function moviments(){

        return $this->hasMany(Moviment::class);

    }

    public function transform()
    {
        
    }
}
