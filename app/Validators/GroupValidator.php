<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class GroupValidator.
 *
 * @package namespace App\Validators;
 */
class GroupValidator extends LaravelValidator
{
    
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name'            => 'required',
            'user_id'         => 'required|exists:users,id' ,        /*Verificação de chave estrangeira*/
            'instituition_id' => 'required|exists:instituitions,id'  /*Verificação de chave estrangeira*/

        ],

        ValidatorInterface::RULE_UPDATE => [],
    ];

    protected $messages = [
        'name.required' => 'We need to know your e-mail address!',
    ];
}
