<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Services\GroupService;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\GroupCreateRequest;
use App\Http\Requests\GroupUpdateRequest;
use App\Repositories\GroupRepository;
use App\Repositories\InstituitionRepository;
use App\Repositories\UserRepository;
use App\Validators\GroupValidator;
use App\Entities\Group;
use Illuminate\Support\Facades\Auth;


class GroupsController extends Controller
{
    
    private $instituitionRepository;
    private $service;
    private $repository;
    private $validator;

    public function __construct(GroupRepository $repository, GroupService $service , InstituitionRepository $instituitionRepository , UserRepository $userRepository)
    {
        $this->repository = $repository;
        $this->service    = $service;
        $this->instituitionRepository = $instituitionRepository;
        $this->userRepository = $userRepository;
    }

   
    public function index()
    {   
        $data['groups']    = $this->repository->all();  
        //$user_list         = \App\Entities\User::pluck('name' , 'id')->all(); Buscar os dados sem L5repository
                    
        //Buscar usando l5 repository
        $user_list = $this->userRepository->selectBoxList();
        $instituition_list = $this->instituitionRepository->selectBoxList(); //Criada na Pasta Repositories

        $data['user_list']         = $user_list;
        $data['instituition_list'] = $instituition_list; 
        return view('group.index', $data);
    }

   
    public function store(GroupCreateRequest $request)
    {
        $request  = $this->service->store( $request->all() );
        $group  = ($request['success'] == true) ? $request['data'] : null;

       
        
        session()->flash('success' , [
            'success' => $request['success'] ,
            'messages'=> $request['messages']
        ]); //Envia a sessão uma única vez para a view
        
        
        return redirect()->route('group.index');
    }

    public function userStore(Request $request , $group_id){

        
        $request  = $this->service->userStore( $group_id , $request->all() );
        
        session()->flash('success' , [
            'success' => $request['success'] ,
            'messages'=> $request['messages']
        ]); //Envia a sessão uma única vez para a view
        
        
        return redirect()->route('group.show' , $group_id);

    }

   
    public function show($id)
    {
        $data['groups']    = $this->repository->find($id);
        $data['user_list'] = $this->userRepository->selectBoxList();

        return view('group.show' , $data);
    }

    
    public function edit($id)
    {   
        $data['group'] = Group::find($id); //OPção sem l5repository
        $data['user_list'] = $this->userRepository->selectBoxList();
        $data['instituition_list'] = $this->instituitionRepository->selectBoxList();

        return view('group.edit' , $data);
    }

    
    public function update(Request $request, $id)
    {
        $request  = $this->service->update( $id , $request->all() );
        
        session()->flash('success' , [
            'success' => $request['success'] ,
            'messages'=> $request['messages']
        ]); //Envia a sessão uma única vez para a view
        
        
        return redirect()->route('group.index');
    }


    
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);
        return redirect()->route('group.index');
    }
}
