<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Services\UserService;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Repositories\UserRepository;
use App\Validators\UserValidator;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    
    protected $repository;
    protected $service;


    public function __construct(UserRepository $repository , UserService $service)
    {
        $this->repository = $repository;
        $this->service    = $service;

    }

   
    public function index()
    {   
        

    
        $users = $this->repository->all();  //Lista todos os usuarios
        $data['users'] = $users;
        //dd(Auth::user()); ou // Auth()->user()

    

        return view('user.index' , $data);
    }

   
    public function store(UserCreateRequest $request)
    {   

        $request  = $this->service->store($request->all());
        $usuario  = ($request['success'] == true) ? $request['data'] : null;

        
        session()->flash('success' , [
            'success' => $request['success'] ,
            'messages'=> $request['messages']
        ]); //Envia a sessão uma única vez para a view


        return redirect()->route('user.index');
        //return view('user.index' , ['usuario' => $usuario]);
    }

   
    public function show($id)
    {
        $user = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $user,
            ]);
        }

        return view('users.show', compact('user'));
    }

    public function edit($id)
    {
        $data['user'] = $this->repository->find($id);

        return view('user.edit', $data);
    }

    
    public function update(Request $request, $id)
    {
        $request  = $this->service->update($request->all() , $id);
        $usuario  = ($request['success'] == true) ? $request['data'] : null;

        
        session()->flash('success' , [
            'success' => $request['success'] ,
            'messages'=> $request['messages']
        ]); //Envia a sessão uma única vez para a view


        return redirect()->route('user.index');
    }


    
    public function destroy($id)
    {
        $request  = $this->service->destroy($id);
        $usuario  = ($request['success'] == true) ? $request['data'] : null;

        session()->flash('success' , [
            'success' => $request['success'] ,
            'messages'=> $request['messages']
        ]); //Envia a sessão uma única vez para a view

        return redirect()->route('user.index');
    }
}
