<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function homepage(){
        $dados['title'] = "Bem-vindo ao investidor legal";
        return view('welcome' , $dados);
    }

    public function cadastrar(){
        echo "Tela de cadastro";
    }

    
    /**
     * Método de login do usuários
     * ===========================
     */
    public function fazerLogin(){
        return view('user.login');
    }
}
