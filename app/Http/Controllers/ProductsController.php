<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\ProductCreateRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Repositories\ProductRepository;
use App\Validators\ProductValidator;
use App\Entities\Instituition;
use App\Services\ProductService;
/**
 * Class ProductsController.
 *
 * @package namespace App\Http\Controllers;
 */
class ProductsController extends Controller
{
    
    protected $repository;

    protected $validator;

    protected $service;

    public function __construct(ProductRepository $repository, ProductService $service , ProductValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->service    = $service;
    }

 
    public function index($instituition_id)
    {   

        $data['instituition'] = Instituition::find($instituition_id);

                
        return view('instituitions.products.index', $data);
    }

    public function store(Request $request , $instituition_id)
    {   
        
        $request  = $this->service->store($request->all() , $instituition_id);
        $instituition  = ($request['success'] == true) ? $request['data'] : null;

        
        session()->flash('success' , [
            'success' => $request['success'] ,
            'messages'=> $request['messages']
        ]); //Envia a sessão uma única vez para a view
        
        //return redirect()->route('instituition.index');
        return redirect()->route('instituition.product.index' , $instituition_id);

    }

   
    public function show($id)
    {
        $product = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $product,
            ]);
        }

        return view('products.show', compact('product'));
    }

    
    public function edit($id)
    {
        $product = $this->repository->find($id);

        return view('products.edit', compact('product'));
    }

    public function update(ProductUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $product = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Product updated.',
                'data'    => $product->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    
    public function destroy($instituition_id , $product_id)
    {
        $deleted = $this->repository->delete($product_id);

        session()->flash('success' , [
            'success' => true ,
            'messages'=> "Produto Removido"
        ]); //Envia a sessão uma única vez para a view

        return redirect()->back();
    }
}
