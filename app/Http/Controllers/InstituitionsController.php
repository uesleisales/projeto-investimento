<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Services\InstituitionService;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\InstituitionCreateRequest;
use App\Http\Requests\InstituitionUpdateRequest;
use App\Repositories\InstituitionRepository;
use App\Validators\InstituitionValidator;

/**
 * Class InstituitionsController.
 *
 * @package namespace App\Http\Controllers;
 */
class InstituitionsController extends Controller
{
   
    protected $repository;
    protected $validator;
    protected $service;

    public function __construct(InstituitionRepository $repository, InstituitionService $service)
    {
        $this->repository = $repository;
        $this->service  = $service;
    }

    public function index()
    {
        
        $data['instituitions'] = $this->repository->all();
        
        return view('instituitions.index', $data);
    }

   
    public function store(InstituitionCreateRequest $request)
    {
        $request  = $this->service->store($request->all());
        $instituition  = ($request['success'] == true) ? $request['data'] : null;

       
        
        session()->flash('success' , [
            'success' => $request['success'] ,
            'messages'=> $request['messages']
        ]); //Envia a sessão uma única vez para a view
        
        //return redirect()->route('instituition.index');
        return redirect()->route('instituition.index');
    }

    
    public function show($id)
    {
        $data['instituition'] = $this->repository->find($id);
        
        return view('instituitions.show' , $data);
        
    }

    
    public function edit($id)
    {
        $data['instituition'] = $this->repository->find($id);

        return view('instituitions.edit', $data);
    }


    public function update(Request $request, $id)
    {
        $request  = $this->service->update($request->all() , $id);
        $instituition  = ($request['success'] == true) ? $request['data'] : null;
       
        
        session()->flash('success' , [
            'success' => $request['success'] ,
            'messages'=> $request['messages']
        ]); //Envia a sessão uma única vez para a view
        
        //return redirect()->route('instituition.index');
        return redirect()->route('instituition.index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Instituition deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Instituition deleted.');
    }
}
