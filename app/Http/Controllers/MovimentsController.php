<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\MovimentCreateRequest;
use App\Http\Requests\MovimentUpdateRequest;
use App\Repositories\MovimentRepository;
use App\Validators\MovimentValidator;
use App\Entities\{Group , Product , Moviment , User};


use Auth;



class MovimentsController extends Controller
{
    /**
     * @var MovimentRepository
     */
    protected $repository;

    /**
     * @var MovimentValidator
     */
    protected $validator;

    /**
     * MovimentsController constructor.
     *
     * @param MovimentRepository $repository
     * @param MovimentValidator $validator
     */
    public function __construct(MovimentRepository $repository, MovimentValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    public function index(){

        return view('moviment.index' ,[
            'product_list' => Product::all() ,
        ]);

    }
    
    public function application(){
        
        $user = Auth()->user();


        $data['group_list']   = ($user != null) ? $user->groups->pluck('name' , 'id') : array() ;  //Alternativa sem o repository
        $data['product_list'] = Product::All()->pluck('name' , 'id');
        


        return view('moviment.application' , $data);
    }

    public function storeApplication(Request $request)
    {   
        $movimento = Moviment::create([
            'user_id'     => Auth::user()->id,     
            'group_id'    =>$request->get('group_id'),
            'product_id'  =>$request->get('product_id'),
            'value'       =>$request->get('value'),
            'type'        => 1
        ]);

        session()->flash('success' , [
            'success' => true,
            'messages'=> "Sua aplicação de R$". $movimento->value ." no produto ". $movimento->product->name ." foi  realizada com sucesso" 
        ]);

        return redirect()->route('moviment.application');
    }


    public function all(){

       $user = Auth()->user();
       $moviment_list = ($user != null) ? $user->moviments : array(); 

        return view('moviment.all' , [
            'moviment_list' => $moviment_list ,
        ]);
    }

    public function getback(){
        $user = Auth()->user();
        
        $data['group_list']   = ($user != null) ? $user->groups->pluck('name' , 'id') : array();  //Alternativa sem o repository
        $data['product_list'] = Product::All()->pluck('name' , 'id');
                
        
        return view('moviment.getback' , $data);
    }

    public function storeGetBack(Request $request)
    {   
        //Verificar se o usuário pode retirar a quantia 
        $user = Auth()->user();
        $entrada = $user->moviments->where('type' , 1)
                            ->where('product_id' , $request->get('product_id'))
                            ->where('group_id'   ,$request->get('group_id'))
                            ->where('user_id' ,Auth::user()->id)
                            ->sum('value');

        $saida =  $user->moviments->where('type' , 2)
                             ->where('product_id' , $request->get('product_id'))
                             ->where('group_id'   ,$request->get('group_id'))
                             ->where('user_id' ,Auth::user()->id)                             
                             ->sum('value');

        
        $total_investido = $entrada - $saida;

        if($total_investido - $request->get('value') < 0 ){
            session()->flash('success' , [
                'success' => true,
                'messages'=> "Erro !! Você não pode retirar uma quantia de R$ ".  $request->get('value') ." nesse produto 
                , no máximo " .$total_investido
            ]);


        }else{
            $movimento = Moviment::create([
                'user_id'     => Auth::user()->id,     
                'group_id'    =>$request->get('group_id'),
                'product_id'  =>$request->get('product_id'),
                'value'       =>$request->get('value'),
                'type'        => 2
            ]);
    
            session()->flash('success' , [
                'success' => true,
                'messages'=> "Seu resgate de  R$ ". $movimento->value ." no produto ". $movimento->product->name ." foi  realizada com sucesso" 
            ]);  
        }

        

        return redirect()->route('moviment.getback');
    }




}
