<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;   //Adicionado
use App\Validators\UserValidator;      //Adicionado
use Exception;                         //Adicionado
use Illuminate\Support\Facades\Auth;
use App\Entities\User;



class DashboardController extends Controller
{   
    private $repository;
    private $validator; 

    public function __construct(UserRepository $repository, UserValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }


    public function index(){
        
        return view('user.dashboard');
    }

    public function auth(Request $requisicao){
        //dd($requisicao->all()); Mostra os dados recebidos

        $data = [
            'email' => $requisicao->get('username'),
            'password' => $requisicao->get('password')
            
        ];
        
        try{

            if(env('PASSWORD_HASH')){
                try{

                    if(Auth::guard('user')->attempt($data, false)){//Classe de autenticação para tentativa . True significa que o usuário será mantido conectado se der sucesso
                        
                        return redirect()->route('dashboard');
                        
                    }else{
                        echo "nao"     ;                
                    }

                }catch(Exception $e){
                    echo "Erro ao se conectar:".$e->getMessage();
                }
            }else{
                
                //Buscando usando o l5 repository
                $user = $this->repository->findWhere($data)->first();
                //$user = $this->repository->findWhere(['email' => $requisicao-get('username')])->first();
                //echo $user->password; 


                if(!$user){ //Se o usuário existe
                    throw new Exception("Credenciais inválidas");
                }else{
                    return redirect()->route('login');
                }

            }

            

        }catch(Exception $e){
            return $e->getMessage();
        }

    }
}
