<?php 

namespace App\Services;
use App\Repositories\ProductRepository;
use App\Validators\ProductValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Illuminate\Database\QueryException;
use Prettus\Validator\Exceptions\ValidatorException;
use Exception;

class ProductService{

    private $repository;
    private $validator;

    public function __construct(ProductRepository $repository , ProductValidator $validator){
        
        $this->repository = $repository;
        $this->validator  = $validator;

    }

    public function store(array $data , $instituition_id){
        try{

            $data['instituition_id'] = $instituition_id;
            $this->validator->with($data)->passesOrfail(ValidatorInterface::RULE_CREATE);
            $product = $this->repository->create($data);

            return [
                'success' => true ,
                'messages'=> "Produto cadastrado com sucesso",
                'data'    => $product
            ];

        }catch(Exception $e){
            switch(get_class($e)){
                case QueryException::class      : return [ 'success' => 'false' , 'data'=> null, 'messages' =>  $e->getMessage()];
                case ValidatorException::class  : return [ 'success' => 'false' , 'data'=> null, 'messages' =>  $e->getMessageBag()];
                case Exception::class           : return [ 'success' => 'false' , 'data'=> null, 'messages' =>  $e->getMessage()];
                default                         : return [ 'success' => 'false' , 'data'=> null, 'messages' =>  $e->getMessage()];
            }
        }
    }


    public function update($data , $id){
        try{

                $this->validator->with($data)->passesOrfail(ValidatorInterface::RULE_UPDATE);
                $instituition = $this->repository->update($data , $id);
    
                return [
                    'success' => true ,
                    'messages'=> "Instituição atualizada com sucesso",
                    'data'    => $instituition
                ];
    
            }catch(Exception $e){
                switch(get_class($e)){
                    case QueryException::class      : return [ 'success' => 'false' , 'data'=> null, 'messages' =>  $e->getMessage()];
                    case ValidatorException::class  : return [ 'success' => 'false' , 'data'=> null, 'messages' =>  $e->getMessageBag()];
                    case Exception::class           : return [ 'success' => 'false' , 'data'=> null, 'messages' =>  $e->getMessage()];
                    default                         : return [ 'success' => 'false' , 'data'=> null, 'messages' =>  $e->getMessage()];
                }
             }
    }


}