<?php 

namespace App\Services;
use App\Repositories\GroupRepository;
use App\Validators\GroupValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Illuminate\Database\QueryException;
use Prettus\Validator\Exceptions\ValidatorException;
use Exception;

class GroupService{
    private $repository;
    private $validator;

    public function __construct(GroupRepository $repository , GroupValidator $validator){
        
        $this->repository = $repository;
        $this->validator  = $validator;

    }

    public function store(array $data){
        try{

            $this->validator->with($data)->passesOrfail(ValidatorInterface::RULE_CREATE);
            $group = $this->repository->create($data);

            return [
                'success' => true ,
                'messages'=> "Grupo cadastrado com sucesso",
                'data'    => $group
            ];

        }catch(Exception $e){
            switch(get_class($e)){
                case QueryException::class      : return [ 'success' => 'false' , 'data'=> null, 'messages' =>  $e->getMessage()];
                case ValidatorException::class  : return [ 'success' => 'false' , 'data'=> null, 'messages' =>  $e->getMessageBag()];
                case Exception::class           : return [ 'success' => 'false' , 'data'=> null, 'messages' =>  $e->getMessage()];
                default                         : return [ 'success' => 'false' , 'data'=> null, 'messages' =>  $e->getMessage()];
            }
        }
    }


    public function userStore($group_id , $data){


        try{

            $group = $this->repository->find($group_id);
            $user_id = $data['user_id'];

            $group->users()->attach($user_id);
            

            return [
                'success' => true ,
                'messages'=> "Usuário relacionado com sucesso",
                'data'    => $group
            ];

        }catch(Exception $e){
            
            dd($e);
            switch(get_class($e)){
                case QueryException::class      : return [ 'success' => 'false' , 'data'=> null, 'messages' =>  $e->getMessage()];
                case ValidatorException::class  : return [ 'success' => 'false' , 'data'=> null, 'messages' =>  $e->getMessageBag()];
                case Exception::class           : return [ 'success' => 'false' , 'data'=> null, 'messages' =>  $e->getMessage()];
                default                         : return [ 'success' => 'false' , 'data'=> null, 'messages' =>  $e->getMessage()];
            }
        }
    }



    public function update($id , $data){
        try{
            
            $this->validator->with($data)->passesOrfail(ValidatorInterface::RULE_UPDATE);
            $group = $this->repository->update($data , $id);

            return [
                'success' => true ,
                'messages'=> "Grupo atualizado com sucesso",
                'data'    => $group
            ];
    
            }catch(Exception $e){
                switch(get_class($e)){
                    case QueryException::class      : return [ 'success' => 'false' , 'data'=> null, 'messages' =>  $e->getMessage()];
                    case ValidatorException::class  : return [ 'success' => 'false' , 'data'=> null, 'messages' =>  $e->getMessageBag()];
                    case Exception::class           : return [ 'success' => 'false' , 'data'=> null, 'messages' =>  $e->getMessage()];
                    default                         : return [ 'success' => 'false' , 'data'=> null, 'messages' =>  $e->getMessage()];
                }
            }
    }
}