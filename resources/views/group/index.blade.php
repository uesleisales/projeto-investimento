@extends('templates.master')

@section('conteudo-view')

@if(session('success'))
    <h3>{{ session('success')['messages'] }}</h3>
@endif

{!! Form::open(['route'=> 'group.store' , 'method' => 'POST' , 'class' => 'form-padrao']) !!}

@include('group.form-fields')

@include('templates.formulario.submit', ['input' => 'Cadastrar'])

{!! Form::close() !!}

<!-- Listagem de grupos -->
@include('group.list' , ['var' => $groups])

@endsection