@extends('templates.master')

@section('conteudo-view')
<header>
    <h1>Nome do grupo: {{ $groups->name }}</h1>
    <h2>Instituição:   {{ $groups->instituition->name }}</h2>
    <h2>Responsável:   {{ $groups->user->name }}</h2>
</header>


{!! Form::open(['route' => ['group.user.store' , $groups->id] , 'method' => 'POST' , 'class' => 'form-padrao']) !!}
@include('templates.formulario.select' , ['label'  => 'Usuário' ,
                                          'select' => 'user_id' ,
                                          'data'   => $user_list ,
                                          'attributes' => ['placeholder' => 'Nome do grupo' , 'autocomplete' => 'off'] ])

@include('templates.formulario.submit' , ['input' => 'Relacionar ao grupo: ' .$groups->name])
{!! Form::close()!!}

@include('user.list' , ['var' => $groups->users])

@endsection