<table class="default-table">
     <thead>
       <tr>
         <td>#</td>
         <td>Nome do grupo</td>
         <td>Patrimônio</td>
         <td>Responsável</td>
         <td>Instituição</td>
         <td>Opções</td>
       </tr>
     </thead>

      
     <tbody>
       <!-- Esse relacionamento $instituition->groups vem da Entitie Instituition-->
       @foreach($var as $gru)
       
       <tr>
             
         <td>{{ $gru->id }}</td>
         <td>{{ $gru->name }}</td>
         <td>{{ number_format($gru->total_value , 2 , ',' , '.') }}</td> <!-- Isso foi criado no model de grupos -->
         <td>{{ $gru->user->name }}</td>
         <td>{{ $gru->instituition->name }}</td>
         <td>{!! Form::open(['route' => ['group.destroy' , $gru->id] , 'method' => 'DELETE' ]) !!}
              {!! Form::submit('Remover') !!}
              {!! Form::close() !!}
            
              <a href="{{ route('group.show' , $gru->id) }}">Detalhes</a>
              <a href="{{ route('group.edit' , $gru->id) }}">Editar</a>

            
         </td>
       </tr>
       @endforeach
      

     </tbody>
  </table>