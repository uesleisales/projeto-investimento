@extends('templates.master')

@section('conteudo-view')
{!! Form::model($group , ['route'=> ['group.update' , $group->id] , 'method' => 'put' , 'class' => 'form-padrao']) !!}
@include('group.form-fields')
@include('templates.formulario.submit', ['input' => 'Atualizar'])

{!! Form::close() !!}
@endsection

