<nav id="principal">

	<ul>
		<li>
			<a href="">
				<i ></i>
				<h3 style="margin-left: -50px;color: yellow;">InvestidorLegal</h3>
			</a>
		</li>
	</ul>

	<ul>
		<li>
			<a href="{{ route('user.index')}}">
				<i class="fa fa-address-book"  aria-hidden="true"></i>
				<h3>Usuários</h3>
			</a>
		</li>
	</ul>
	

	<ul>
		<li>
			<a href="{{ route('instituition.index') }}">
				<i class="fa fa-building" aria-hidden="true"></i>
				<h3>Instituições</h3>
			</a>
		</li>
	</ul>
	
	<ul>
		<li>
			<a href="{{ route('group.index') }}">
				<i class="fa fa-users" aria-hidden="true"></i>
				<h3>Grupos</h3>
			</a>
		</li>
	</ul>


	<ul>
		<li>
			<a href="{{ route('moviment.application') }}">
				<i class="fa fa-money" aria-hidden="true"></i>
				<h3>Investir</h3>
			</a>
		</li>
	</ul>

	<ul>
		<li>
			<a href="{{ route('moviment.getback') }}">
				<i class="fa fa-share" aria-hidden="true"></i>
				<h3>Resgatar</h3>
			</a>
		</li>
	</ul>


	<ul>
		<li>
			<a href="{{ route('moviment.index') }}">
				<i class="fa fa-dollar" aria-hidden="true"></i>
				<h3>Aplicações</h3>
			</a>
		</li>
	</ul>

	<ul>
		<li>
			<a href="{{ route('moviment.all') }}">
				<i class="fa fa-exchange" aria-hidden="true"></i>
				<h3>Extrato</h3>
			</a>
		</li>
	</ul>

</nav>