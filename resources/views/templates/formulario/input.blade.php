<label class="{{ $class ?? null }}">
    <span>{{ $label ?? "ERRO" }}</span>
    {!! Form::text($input , $value ?? null , $attributes ) !!}
</label>