<label class="{{ $class ?? null }}">
    <span>{{ $input ?? "ERRO" }}</span>
    {!! Form::password($input , $attributes ) !!}
</label>