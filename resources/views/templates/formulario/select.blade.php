<label class="{{ $class ?? null }}">
    <span>{{ $label ?? "ERRO" }}</span>
    {!! Form::select($select , $data ?? []) !!}
</label>