
<!-- A estrutura de cadastro e atutalização vão usar esses campos -->
@include('templates.formulario.input' , [ 'label' => 'CPF' , 'input' => 'cpf' , 'attributes' => ['placeholder' => 'CPF' , 'autocomplete' => 'off']])
@include('templates.formulario.input' , [ 'label' => 'NOME' , 'input' => 'name' , 'attributes' => ['placeholder' => 'Nome' , 'autocomplete' => 'off']])
@include('templates.formulario.input' , [ 'label' => 'TELEFONE' , 'input' => 'phone' , 'attributes' => ['placeholder' => 'Telefone' , 'autocomplete' => 'off']])
@include('templates.formulario.input' , [ 'label' => 'E-MAIL' , 'input' => 'email' , 'attributes' => ['placeholder' => 'E-mail' , 'autocomplete' => 'off']])
@include('templates.formulario.password', [ 'label' => 'SENHA' , 'input' => 'password' , 'attributes' => ['placeholder' => 'Senha' , 'autocomplete' => 'off']])
