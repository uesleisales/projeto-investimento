@extends('templates.master')


@section('css-view')
@endsection

@section('conteudo-view')
  

  @if(session('success'))
    <h3>{{ session('success')['messages'] }}</h3>
  @endif


  {!! Form::open(['route' => 'user.store' , 'method' => 'post' , 'class'=> 'form-padrao' ])!!}
    @include('user.form-fiels')
    @include('templates.formulario.submit', ['input' => 'Cadastrar'])

  {!! Form::close() !!}
  
  @include('user.list' , ['var' => $users])

@endsection

@section('js-view')
@endsection