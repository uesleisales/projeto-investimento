@extends('templates.master')


@section('conteudo-view')

@if(session('success'))
    <h3>{{ session('success')['messages'] }}</h3>
@endif


{!! Form::open(['route' => ['instituition.product.store' , $instituition->id]  , 'method' => 'post' , 'class' => 'form-padrao']) !!}
    
    @include('templates.formulario.input' , [
        'label' => 'Nome' ,
        'input' => 'name' , 
        'attributes' => ['autocomplete' => 'off' , 'placeholder' => 'Nome do produto' , 'autocomplete' => 'off']
    ])
    
    @include('templates.formulario.input' , [
        'label' => 'Descrição' ,
        'input' => 'description' , 
        'attributes' => ['autocomplete' => 'off' , 'placeholder' => 'Descrição do produto' , 'autocomplete' => 'off']
    ])

    @include('templates.formulario.input' , [
        'label' => 'Indexador' ,
        'input' => 'index' , 
        'attributes' => ['autocomplete' => 'off' , 'placeholder' => 'Indexador' , 'autocomplete' => 'off']
    ])

    @include('templates.formulario.input' , [
        'label' => 'Taxa de juros' ,
        'input' => 'interest_rate' , 
        'attributes' => ['autocomplete' => 'off' , 'placeholder' => 'Taxa' , 'autocomplete' => 'off']
    ])

    @include('templates.formulario.submit', ['input' => 'Cadastrar'])

{!! Form::close() !!}


<table class="default-table">
    <thead>
        <th>#</th>
        <th>Nome</th>
        <th>Descrição</th>
        <th>Indexador</th>
        <th>Taxa</th>
        <th>Opções</th>
    </thead>
    <tbody>
    @forelse($instituition->products as $product)
        <tr>
            <td>{{ $product->id }}</td>
            <td>{{ $product->name }}</td>
            <td>{{ $product->description }}</td>
            <td>{{ $product->index }}</td>
            <td>{{ $product->interest_rate }}</td>
            <td>
                <a href="">Editar</a>
                {!! Form::open(['route' => ['instituition.product.destroy' , $instituition->id , $product->id] , 'method' => 'DELETE' ]) !!}
                {!!Form::submit('Remover') !!}
                {!! Form::close() !!}


            </td>
        </tr>
    @empty
        <tr>
            <td>Nada cadastrado</td>
        </tr>
    @endforelse

    </tbody>
</table>


@endsection
