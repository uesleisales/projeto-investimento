@extends('templates.master')

@section('conteudo-view')

<header> 
    <h1>{{ $instituition->name }}</h1>
</header>

<!-- Tabela de listagem dinâmica -->
@include('group.list' , ['var' => $instituition->groups])

@endsection