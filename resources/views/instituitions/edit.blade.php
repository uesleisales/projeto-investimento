@extends('templates.master')

@section('conteudo-view')
    @if(session('success'))
        <h3>{{ session('success')['messages'] }}</h3>
    @endif

    {!! Form::model( $instituition , ['route' => ['instituition.update' , $instituition->id] , 'method' => 'put' , 'class' => 'form-padrao']) !!} 
    @include('templates.formulario.input' , [ 'Nome' => 'Instituição' , 'input' => 'name' , 'attributes' => ['placeholder' => 'Nome' , 'autocomplete' => 'off']])

    @include('templates.formulario.submit', ['input' => 'Atualizar'])
    {!! Form::close() !!}
@endsection